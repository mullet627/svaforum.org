import { ParsedContent } from '@nuxt/content'

export enum PanelistPosition {
  'affirmative',
  'negative'
}

export interface Panelist {
  name: string
  title: string
  org: string
  orgurl: string
  location: string
  position: PanelistPosition
}

export interface PanelEvent extends ParsedContent {
  id: string
  name: string
  date: string
  title: string
  banner: string
  moderator: string
  panelists: Panelist[]
  youtube: string
  rsvp: string
  _file?: string
}
