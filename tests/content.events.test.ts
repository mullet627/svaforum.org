/* eslint-env jest */
import slugify from '@sindresorhus/slugify'

import { readEvents, PanelEvent, PanelistPosition } from '../lib/events'
import { readImages, ImageInfo } from '../lib/images'

const LONG_TIMEOUT = 30 * 1000

describe('events', () => {
  let events: PanelEvent[]
  let images: ImageInfo[]

  beforeAll(async () => {
    events = await readEvents()
    images = await readImages('events', '*/*.jpg')
  }, LONG_TIMEOUT)

  it('fields', () => {
    for (const e of events.filter(e => e.date)) {
      expect(e.moderator, e._file).toBeTruthy()
      expect(e.rsvp, e._file).toBeTruthy()
    }
    for (const e of events.filter(e => !e.date)) {
      expect(e.moderator, e._file).not.toBeTruthy()
      expect(e.rsvp, e._file).not.toBeTruthy()
    }
  })

  it('panelists present', () => {
    for (const e of events.filter(e => (e.panelists && e.panelists.length))) {
      expect(e.panelists.length, e.id).toBe(2)
      expect(e.panelists[0].position, `${e.id} 0`).toEqual(PanelistPosition.Affirmative)
      expect(e.panelists[1].position, `${e.id} 1`).toEqual(PanelistPosition.Negative)
    }
  })

  it('images exist', () => {
    for (const e of events) {
      for (const p of e.panelists.filter(p => !!p.name)) {
        const jpg = `${e.id}/${slugify(p.name)}.jpg`
        const msg = `${e._file} ${p.name}`
        expect(images.find(i => i.path === jpg), msg).toBeTruthy()
      }
    }
  })

  it('no extra images', () => {
    for (const i of images) {
      const m = i.path.match(/(.*)\/(.*).jpg/)!
      const e = events.find(e => e.id === m[1])!
      expect(e, i.path).toBeTruthy()
      const p = e.panelists.find(p => slugify(p.name) === m[2])
      expect(p, i.path).toBeTruthy()
    }
  })

  it('1080x1350', () => {
    for (const i of images) {
      expect(i.width, i.path).toBe(1080)
      expect(i.height, i.path).toBe(1350)
    }
  })

  it('<250kb', () => {
    for (const i of images) {
      expect(i.size, i.path).toBeLessThan(250 * 1024)
    }
  })
})
