/* eslint-env jest */
import { readImages, ImageInfo } from '../lib/images'

const LONG_TIMEOUT = 30 * 1000

describe('banners', () => {
  let banners : ImageInfo[]

  beforeAll(async () => {
    banners = await readImages('banners', '*/*.jpg')
  }, LONG_TIMEOUT)

  it('1920x1080', () => {
    for (const b of banners) {
      expect(b.width, b.path).toBe(1920)
      expect(b.height, b.path).toBe(1080)
    }
  })

  it('<250kb', () => {
    for (const b of banners) {
      expect(b.size, b.path).toBeLessThan(250 * 1024)
    }
  })
})
