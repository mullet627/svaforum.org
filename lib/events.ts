import fs from 'fs/promises'
import { ParsedContent } from '@nuxt/content/dist/runtime/types'
import { globby } from 'globby'
/// <reference path="../node_modules/@nuxt/content/dist/runtime/transformers/index.d.ts" />
import { transformContent } from '../node_modules/@nuxt/content/dist/runtime/transformers'
import { contentDir } from './projpath'

export enum PanelistPosition {
  Affirmative = 'affirmative',
  Negative = 'negative'
}

export interface Panelist {
  name: string
  title: string
  org: string
  orgurl: string
  location: string
  position: PanelistPosition
}

export interface PanelEvent extends ParsedContent {
  id: string
  name: string
  date: string
  title: string
  banner: string
  moderator: string
  panelists: Panelist[]
  youtube: string
  rsvp?: string
  _file?: string
}

export async function readEvents (): Promise<PanelEvent[]> {
  const events: PanelEvent[] = []
  for (const p of await globby(contentDir('events', '*.md'))) {
    const content = await fs.readFile(p, 'utf-8')
    events.push(await transformContent(`test:${p}`, content) as PanelEvent)
  }
  return events
}

export async function rsvpRedirects () {
  return (await readEvents())
    .filter(e => e.rsvp)
    .map(e => [`/rsvp/${e.id}`, e.rsvp, 301])
}
