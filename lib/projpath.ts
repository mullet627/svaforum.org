import path from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const ROOT_DIR = path.join(path.dirname(__filename), '..')
const PUBLIC_DIR = path.join(ROOT_DIR, 'public')
const CONTENT_DIR = path.join(ROOT_DIR, 'content')

export function publicDir (...args: string[]): string {
  return path.join(PUBLIC_DIR, ...args)
}

export function contentDir (...args: string[]): string {
  return path.join(CONTENT_DIR, ...args)
}
