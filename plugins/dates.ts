import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'
import timezone from 'dayjs/plugin/timezone.js'
import { PanelEvent } from '~/lib/events'

// eslint-disable-next-line import/no-named-as-default-member
const { extend: dayjsExtend } = dayjs

dayjsExtend(utc)
dayjsExtend(timezone)

export default defineNuxtPlugin(() => {
  return {
    provide: {
      fancyDate (e: PanelEvent) {
        return dayjs.tz(e.date, 'America/Los_Angeles')
          .format('dddd, MMMM D, YYYY')
      }
    }
  }
})
