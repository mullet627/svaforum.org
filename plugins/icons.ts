import Close from 'vue-material-design-icons/Close.vue'
import Email from 'vue-material-design-icons/Email.vue'
import EmailNewsletter from 'vue-material-design-icons/EmailNewsletter.vue'
import Facebook from 'vue-material-design-icons/Facebook.vue'
import MapMarker from 'vue-material-design-icons/MapMarker.vue'
import MicrophoneVariant from 'vue-material-design-icons/MicrophoneVariant.vue'
import OpenInNew from 'vue-material-design-icons/OpenInNew.vue'
import Play from 'vue-material-design-icons/Play.vue'
import Youtube from 'vue-material-design-icons/Youtube.vue'

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component('CloseIcon', Close)
  nuxtApp.vueApp.component('EmailIcon', Email)
  nuxtApp.vueApp.component('EmailNewsletterIcon', EmailNewsletter)
  nuxtApp.vueApp.component('FacebookIcon', Facebook)
  nuxtApp.vueApp.component('MapMarkerIcon', MapMarker)
  nuxtApp.vueApp.component('MicrophoneVariantIcon', MicrophoneVariant)
  nuxtApp.vueApp.component('OpenInNewIcon', OpenInNew)
  nuxtApp.vueApp.component('PlayIcon', Play)
  nuxtApp.vueApp.component('YoutubeIcon', Youtube)
})
