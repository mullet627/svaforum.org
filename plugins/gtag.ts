import VueGtag from 'vue-gtag'

// FUTURE(vue-gtag>2.0.1): Remove this
// https://github.com/MatteoGabriele/vue-gtag/pull/537
declare module 'vue-gtag' {
  interface PluginOptions {
    deferScriptLoad?: boolean
  }
}

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(VueGtag, {
    deferScriptLoad: true,
    config: {
      id: 'G-TCWV7EMH9B'
    }
  })
})
