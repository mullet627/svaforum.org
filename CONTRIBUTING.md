# Contribution Guidelines

## How to make a change

Important: Make sure you fork the project by clicking the `fork` button on the
[project page](https://gitlab.com/svrbc/svaforum.org) before trying to push it.

    export GITLAB_USERNAME=$USER # or whatever your username is.
    git fetch
    git checkout -b mychange origin/master
    # Make changes.
    git commit -a
    # Test your changes before pushing.
    yarn test
    git push https://gitlab.com/$GITLAB_USERNAME/svaforum.org.git -o merge_request.create -o merge_request.remove_source_branch
    # Make more changes, but keep it all as a single commit.
    git commit -a --amend
    # Push the change again
    git push -f https://gitlab.com/$GITLAB_USERNAME/svaforum.org.git

You can alias the `git push ...` commands to `glmr` by adding the following to
your `~/.bashrc` and running `. ~/.bashrc`.

    GITLAB_USERNAME=$USER # Or whatever your username is
    alias glmr='git push -f $(git config --get remote.origin.url | sed "s/\(^https:\/\/gitlab.com\/\)[^/]*\//\1$GITLAB_USERNAME\//") -o merge_request.create -o merge_request.remove_source_branch'
