# svaforum.org

## Getting the code

    git clone gitlab.com/svrbc/svaforum.org
    cd svaforum.org

## Installing node

Install `nvm`:

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    nvm install
    nvm use

## Running the server

    npm install -g yarn
    yarn install
    yarn dev -o

## Deploying the project

    yarn generate
    yarn deploy

## Adding a new event

Add a file to `content/events/` (see that directory for examples)

If you want to add a new banner image:

- Place it in `/public/banners`.
- It must be 1920x1080
- It must be under 250kb

## Updating the homepage

The homepage is rebuilt

- [on every merge](https://gitlab.com/svrbc/svaforum.org/-/merge_requests),
- [on Saturday nights at 9:00](https://gitlab.com/svrbc/svaforum.org/-/pipeline_schedules/302241/edit), and
- [on manual rebuilds](https://gitlab.com/svrbc/svaforum.org/-/pipelines/new).

See .gitlab-ci.yml
