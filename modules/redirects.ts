import fs from 'fs/promises'
import path from 'path'

import { defineNuxtModule, useNitro } from '@nuxt/kit'
import { Nuxt } from '@nuxt/schema'

type RedirectRule = [string, string, number]
type RedirectLine = RedirectRule | (() => RedirectRule[])

declare module '@nuxt/schema' {
  interface NuxtOptions {
    ['redirects']: RedirectLine[]
  }
}

let publicDir: string
export default defineNuxtModule({
  meta: {
    name: '@inkylabs/redirects',
    configKey: 'redirects',
    compatibility: {
      nuxt: '^3.2.3'
    }
  },
  defaults: {},
  hooks: {
    ready () {
      // For some reason nitro isn't still available at close time.
      const nitro = useNitro()
      publicDir = nitro.options.output.publicDir
    },
    async close (nuxt: Nuxt) {
      // We shouldn't make the file if we aren't calling `nuxi generate`
      if (!nuxt.options._generate) return

      const redirects: RedirectRule[] = []
      for (const line of nuxt.options.redirects) {
        if (line instanceof Array) {
          redirects.push(line)
          continue
        }
        redirects.push(...await line())
      }

      const fp = path.join(publicDir, '_redirects')
      await fs.writeFile(fp, redirects
        .map(l => l.join('\t'))
        .join('\n')
      )
    }
  }
})
