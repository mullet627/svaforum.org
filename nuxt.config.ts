import { rsvpRedirects } from './lib/events'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  modules: [
    '@kevinmarrec/nuxt-pwa',
    '@nuxt/content',
    'vue-debug-queryparam/nuxt',
    'vue-konami-utils/nuxt',
    'nuxt-content-prerender',
    '@nuxt/image-edge',
    'nuxt-purgecss',
    './modules/redirects.ts'
  ],
  app: {
    head: {
      htmlAttrs: {
        lang: 'en'
      },
      link: [
        { rel: 'icon', type: 'image/png', href: '/favicon.png' }
      ],
      meta: [
        // name, description, etc. are defined below in the PWA module config
        // section, since that will overwrite of the fields if we set them
        // here.
        { name: 'keywords', content: 'areopagus, silicon valley, forum, debate, panel, discussion, dialogue, christian, christianity, jesus, god, religion, spirituality, philosophy' },
        { name: 'geo.region', content: 'US-CA' },
        { name: 'geo.placename', content: 'Sunnyvale' },
        { name: 'geo.position', content: '37.394709;-122.005382' },
        { name: 'ICBM', content: '37.394709, -122.005382' }
      ]
    }
  },
  css: [
    'vue-material-design-icons/styles.css',
    '@/assets/css/main.scss'
  ],
  konami: {
    youtubeAutoplayId: 'eTXidXsgR4Q',
    youtubeAutoplayTitle: "God's Servant - Areopagus"
  },
  router: {
    options: {
      // Bootstrap expects .active.
      linkActiveClass: 'active'
    }
  },
  redirects: [
    rsvpRedirects,
    ['/favicon.ico', '/favicon.png', 200],
    ['/*', '/notfound', 404]
  ],
  contentPrerender: {
    specs: [
      {
        contentDir: '~/content/pages',
        contentGlob: '**.md',
        routePath: '/:path($_path)',
        componentFile: '~/components/MarkdownPage.vue'
      }
    ]
  },
  pwa: {
    meta: {
      name: 'Silicon Valley Areopagus Forum',
      description: 'A series of moderated discussions between the ' +
        'pastors and thought leaders of Silicon Valley and the larger San ' +
        'Francisco Bay area',
      author: 'Silicon Valley Reformed Baptist Church',
      ogImage: '/home/svaf-motto.jpg'
    }
  },
  typescript: {
    typeCheck: true,
    strict: true
  }
})
