---
title: Info for Organizers
banner: light/arches
---

## Event Prep

<div class="infolistset">
<div class="infolist">

### Info From Panelists

1.  500 word bio
1.  Permission to record and distribute

</div>

<div class="infolist">

### Setup

1.  Arm chairs
1.  Stool
1.  Bottles of water for panelists
1.  Recording
1.  Table for information about SVRBC

</div>

<div class="infolist">

### Volunteers

1.  Food preparation
1.  Photographer
1.  Someone to man the church table
1.  Someone to filter questions / feed them to the moderator

</div>
</div>

## Other-Faith Topics

<div class="infolistset">
<div class="infolist">

### Bibliology

-   Preservation of Scripture
-   Writings of Ellen G White
-   Apocrypha (RC)
-   Tradition (RC)

### Theology proper

-   Pre-existence of Christ
-   Deity of Christ
-   The Harrowing of Hell
-   Personhood of the Holy Spirit
-   Existence of God

</div>

<div class="infolist">

### Anthropology

-   Soul sleep
-   Consciousness and machines
-   Simulation hypothesis
-   Man and immortality
-   Evolution
-   Ethics
-   Homosexuality
-   The legitimacy of oaths
-   Feminism

</div>

<div class="infolist">

### Soteriology

-   Original sin
-   Penal substitution (EO)
-   Sola fide
-   The necessity of baptism (CoC)
-   Ecclesiology
-   The papacy
-   Validity of the Sacraments
-   Transubstantiation
-   Women pastors
-   Community without God

### Eschatology

-   Eternal conscious torment

</div>

<div class="infolist">

### Area religions

-   Hinduism
-   Islam
-   Buddhism
-   Sikhism
-   Bahai-ism
-   Secular camps
-   Scientology
-   Atheism
-   Singularitarianism
-   Christian-derivative sects
-   Christadelphianism
-   Christian Science
-   Mormonism
-   International Church of Christ

</div>
</div>

## In-House Topics

<div class="infolistset">
<div class="infolist">

### Bibliology

-   Apocrypha (Anglican/Lutheran)
-   Speaking in tongues
-   Continuing prophecy
-   The identity of the Nephilim
-   The identity of Michael
-   Ecclesiastical text
-   6 day creation
-   Hesychasm/lectio divina
-   Evidentialism
-   Classical apologetics
-   Scripturalism

</div>

<div class="infolist">

### Theology proper

-   Eternal functional subordination
-   Filioque

### Anthropology

-   Trichotomy
-   Animal souls

</div>

<div class="infolist">

### Soteriology

-   Scope of the atonement
-   Election
-   Prevenient grace
-   Libertarian free will
-   Well-meant offer
-   Direct imputation of sin
-   Propositionalism
-   Ethics
-   Threefold division of the law
-   Divorce and remarriage
-   Theonomy
-   Complementarianism
-   Images of Christ
-   Alcohol

</div>

<div class="infolist">

### Ecclesiology

-   Children's church
-   Elder rule
-   Presbyteries
-   Episcopal government
-   Parity of elders
-   Connectionalism
-   VR church
-   Satellite churches
-   Segregated churches
-   Home churches
-   Vocational pastors
-   Scope of the Noahic covenant
-   Church membership
-   Confessionalism
-   Regulative principle

</div>

<div class="infolist">

### Worship

-   Regulative principle
-   Exclusive psalmody
-   Instruments in worship
-   Sacramentology
-   Transubstantiation
-   Intinction
-   Hebrews 8 and baptism
-   Communion in small groups
-   Who can administer the sacraments

</div>

<div class="infolist">

### Eschatology
-   Dispensational view of Israel
-   Historic premillenialism
-   Postmillenialism

### Society

-   Reconstructionism
-   Two Kingdoms theology
-   Voluntarism
-   Reparations

</div>
</div>

## Press Material

![logo](/logo.png)

![logo boxes](/logo-boxes.png)
