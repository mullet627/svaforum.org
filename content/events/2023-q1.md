---
id: 2023-q1
date: 2023-02-25
title: Is Online Church Real Church?
banner: dark/servers
moderator: Conley Owens
panelists:
  - name: Kaloma Smith
    title: Pastor
    org: University A.M.E. Zion Church
    orgurl: https://universityamez.com/
    location: Palo Alto
    position: affirmative
  - name: Stephen Louis
    title: Pastor
    org: Sovereign Grace Baptist Church of Silicon Valley
    orgurl: http://www.sgbcsv.org
    location: Morgan Hill
    position: negative
youtube: rJTnenD8UKw
rsvp: https://www.eventbrite.com/e/panel-discussion-is-online-church-real-church-tickets-491495735977
---

People use the internet to attend social events, attend school, even attend
their jobs. But is church still church if it’s online? Come join us as we
discuss this important and timely question!
