import { computed, onMounted } from 'vue'

export default () => {
  const origin = useState('origin', () => '')
  onMounted(() => (origin.value = window.location.origin))
  return computed(() => origin.value)
}
