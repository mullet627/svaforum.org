import { computed, onMounted } from 'vue'

export default () => {
  const mounted = useState('mounted', () => false)
  onMounted(() => (mounted.value = true))
  return computed(() => mounted.value)
}
